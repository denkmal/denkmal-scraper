import {TomlReader} from "@sgarciac/bombadil";
import * as de from '@mojotech/json-type-validation';
import fs from "fs";
import util from "util";
import {Region, RegionVariant} from "./denkmal_data/region";
import {Coordinate} from "tsgeo/Coordinate";
import {LoggerOptions} from "./logger";
import {BaseError} from "./error/baseError";
import moment, {Duration} from "moment-timezone";

export {loadConfig, Config}

type Config = {
    days_to_fetch: number,
    run_every: Duration,
    logging: LoggerOptions,
    denkmal_api: {
        url: string,
        auth_token: string,
    },
    regions: Region[];
}

function loadConfig(path: string): Config {
    let json: object = loadToml(path);
    let decoder: de.Decoder<Config> = de.object({
        days_to_fetch: de.number(),
        run_every: de.string().map(v => moment.duration(v)),
        logging: de.object({
            console: de.object({
                level: de.string(),
            }),
            elasticsearch: de.optional(de.object({
                level: de.string(),
                host: de.string(),
            })),
        }),
        denkmal_api: de.object({
            url: de.string(),
            auth_token: de.string(),
        }),
        regions: de.dict(
            de.object({
                id: de.string(),
                coord: de.object({lat: de.number(), lng: de.number()}),
                timeZone: de.string(),
            })
        ).andThen<Region[]>(v => {
            let regions = [];
            for (let name in v) {
                if (!(name in RegionVariant)) {
                    return de.fail(`Invalid region name: ${name}`);
                }
                let data = v[name];
                let region = new Region(data.id, name as RegionVariant, data.timeZone, new Coordinate(data.coord.lat, data.coord.lng));
                regions.push(region);
            }
            return de.succeed(regions);
        }),
    });
    let config = decoder.runWithException(json);
    config = replaceEnvPlaceholders(config);
    return config;
}

function loadToml(path: string): object {
    let contents = fs.readFileSync(path).toString();
    let reader = new TomlReader();
    reader.readToml(contents);
    if (reader.errors.length > 0) {
        throw new Error(`Failed to read config '${path}':\n ${util.inspect(reader.errors)}`);
    }
    let result = reader.result;
    return JSON.parse(JSON.stringify(result)); // Make it a standard object
}

function replaceEnvPlaceholders(c: Config): Config {
    if (c.logging.elasticsearch) {
        c.logging.elasticsearch.host = replaceEnv(c.logging.elasticsearch.host);
        if (c.logging.elasticsearch.host.length === 0) {
            c.logging.elasticsearch = undefined
        }
    }
    c.denkmal_api.auth_token = replaceEnv(c.denkmal_api.auth_token);
    return c;
}

function replaceEnv(v: string): string {
    return v.replace(/\${([^}]+)}/, (_, name) => {
        let value = process.env[name];
        if (value === undefined) {
            throw new BaseError(`Environment variable '${name}' not set, but used in config.`);
        }
        return value;
    });
}
