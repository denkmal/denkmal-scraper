import {Config, loadConfig} from "./config";
import {Level} from "./logger";


test('loadConfig', () => {
    process.env.ELASTICSEARCH_URL = 'http://my-elasticsearch:9200';
    process.env.DENKMAL_API_TOKEN = 'my-api-token';

    let config: Config = loadConfig('./config/config.toml');

    expect(config).toHaveProperty('regions');
    expect(config.logging.elasticsearch).toEqual({
        level: Level.info,
        host: 'http://my-elasticsearch:9200',
    });
    expect(config.denkmal_api.auth_token).toEqual('my-api-token');
});

test('loadConfig with empty ELASTICSEARCH_URL', () => {
    process.env.ELASTICSEARCH_URL = '';
    process.env.DENKMAL_API_TOKEN = 'my-api-token';

    let config: Config = loadConfig('./config/config.toml');

    expect(config.logging.elasticsearch).toBeUndefined();
});
