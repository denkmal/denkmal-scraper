import {DenkmalRepo} from "./repo";
import {Region, RegionVariant} from "./region";
import {Coordinate} from "tsgeo/Coordinate";
import {Venue} from "./venue";

describe('Repo', () => {
    let repo: DenkmalRepo;
    beforeEach(() => {
        let regions = [
            new Region('id-basel', RegionVariant.basel, 'Europe/Zurich', new Coordinate(47.55457, 7.59032)),
            new Region('id-zurich', RegionVariant.zurich, 'Europe/Zurich', new Coordinate(47.37723, 8.52735)),
        ];
        let venues = [
            new Venue('id-1', 'Venue 1', RegionVariant.basel),
            new Venue('id-2', 'Venue 2', RegionVariant.zurich),
            new Venue('id-3', 'Venue 3', RegionVariant.zurich, 'fb-1234'),
        ];
        repo = new DenkmalRepo(regions, venues);
    });

    test('getRegionByVariant', () => {
        let region = repo.getRegionByVariant(RegionVariant.basel);
        expect(region).toBeInstanceOf(Region);
        expect(region.id).toEqual('id-basel');
    });

    test('getRegionByVariant not found', () => {
        expect(() => {
            repo.getRegionByVariant('foo' as RegionVariant);
        }).toThrow(/Cannot find item/);
    });

    test('getRegionById', () => {
        let region = repo.getRegionById('id-zurich');
        expect(region).toBeInstanceOf(Region);
        expect(region.variant).toEqual(RegionVariant.zurich);
    });

    test('getRegionById not found', () => {
        expect(() => {
            repo.getRegionById('foo' as RegionVariant);
        }).toThrow(/Cannot find item/);
    });
});
