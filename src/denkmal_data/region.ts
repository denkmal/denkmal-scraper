import {Coordinate} from "tsgeo/Coordinate";
import {Vincenty} from "tsgeo/Distance/Vincenty";

export {Region, RegionVariant}

enum RegionVariant {
    basel = 'basel',
    zurich = 'zurich',
}

class Region {
    id: string;
    variant: RegionVariant;
    coord: Coordinate;
    timeZone: string;

    constructor(id: string, name: RegionVariant, timeZone: string, coord: Coordinate) {
        this.id = id;
        this.variant = name;
        this.coord = coord;
        this.timeZone = timeZone;
    }

    getDistance(coord: Coordinate): number {
        let calculator = new Vincenty();
        return calculator.getDistance(this.coord, coord);
    }
}
