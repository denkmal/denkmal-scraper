import {Region, RegionVariant} from "./region";
import {Coordinate} from "tsgeo/Coordinate";

describe('Region', () => {

    test('getDistance', () => {
        let basel = new Region("id-1", RegionVariant.basel, "Europe/Zurich", new Coordinate(47.55814, 7.58769));

        let weil = new Coordinate(47.5926466, 7.6195624);
        expect(basel.getDistance(weil)).toBe(4524.264);
        expect(basel.getDistance(weil)).toBeCloseTo(5_000, -3);

        let oerlikon = new Coordinate(47.416667, 8.55);
        expect(basel.getDistance(oerlikon)).toBeCloseTo(74_000, -3);
    });
});