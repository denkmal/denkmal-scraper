import { HttpError } from "error/httpError";
import { $load } from "./cheerio/load";
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import axiosRetry from 'axios-retry';

export { fetchUrl, getHtml }

async function getHtml(url: string) {
    return $load(await fetchUrl(url)).root();
}

async function fetchUrl(url: string, options: FetchOptions = {}): Promise<any> {
    let axiosOptions: AxiosRequestConfig = {
        timeout: 1000 * 30,
        headers: {
            'User-Agent': 'Mozilla/5.0 AppleWebKit',
        },
    };

    if (options.method) {
        axiosOptions.method = options.method;
    }
    if (options.body) {
        axiosOptions.data = options.body;
    }
    if (options.headers) {
        axiosOptions.headers = { ...axiosOptions.headers, ...options.headers };
    }

    axiosRetry(axios, { retries: 3, retryDelay: axiosRetry.exponentialDelay });
    try {
        let response: AxiosResponse = await axios(url, axiosOptions);
        return response.data;
    } catch (e: any) {
        let extra: { [key: string]: string } = {};
        if (e.response) {
            extra['responseStatus'] = [e.response.status, e.response.statusText].filter(s => !!s).join(' ');
            extra['responseBody'] = e.response.data;
        }
        throw new HttpError(`Failed to fetch '${url}': ${e.message}`, extra);
    }
}

type FetchOptions = {
    method?: 'GET' | 'POST';
    body?: string;
    headers?: { [key: string]: string },
};