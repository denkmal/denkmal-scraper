import moment from 'moment-timezone';
import nock from 'nock';
import {EventTime} from "source/eventTime";


// Set a non-standard timezone, to avoid tests from depending on the local timezone.
moment.tz.setDefault("America/New_York");


// Disallow netwok requests during tests
nock.disableNetConnect();


// Jest snapshot serializer for `EventTime`
expect.addSnapshotSerializer({
    test: (val) => val instanceof EventTime,
    print: (val) => (val as EventTime).toStringDebug(),
});
