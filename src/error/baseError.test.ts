import {BaseError} from "./baseError";
import {ParseError} from "./parseError";

describe('BaseError', () => {

    test('instanceof', () => {
        let error = new BaseError("My Error");
        expect(error).toBeInstanceOf(BaseError);
        expect(error).toBeInstanceOf(Error);
    });

    test('message', () => {
        let error = new BaseError("My Error");
        expect(error.message).toBe("My Error");
    });

    test('extra', () => {
        let extra = {foo1: "jo", "foo2": 12};
        let error = new BaseError("My Error", extra);
        expect(error.extra).toBe(extra);
    });

    test('fromError', () => {
        let original = new Error('My error');
        let copy = ParseError.fromError(original);
        expect(copy.message).toBe(original.message);
        expect(copy).toBeInstanceOf(ParseError);
    });

});
