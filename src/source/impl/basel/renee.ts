import {Source, SourcePriority} from "source/source";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import {FetchRequest} from "runner";
import {EventData, EventDataBuilder} from "source/eventData";
import {joinComma} from "source/textHelper";
import {fetchUrl} from "fetchUrl";
import {$load, $loadElement} from "../../../cheerio/load";


export class Renee implements Source {
    sourceName(): string {
        return "renee";
    }

    regionVariant(): RegionVariant {
        return RegionVariant.basel;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Renee()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let urlBase = `https://www.renee.ch`;
        let html = await fetchUrl(urlBase);
        let $ = $load(html);

        let $events = $('.event').map((i, $e) => $loadElement(($e))).toArray();
        return $events.map($event => {
            let startDate = $event.find('[itemprop="startDate"]').attr('content')!;
            let description = $event.find('.description').textVisual();

            let textLines = description.split('\n');
            let genres = null;
            if (textLines.length > 1) {
                genres = textLines.pop();
            }

            let event = new EventDataBuilder(request.now, "https://www.renee.ch");
            event.venueName = "Renée";
            event.time.from.setByISO8601(startDate);
            event.description = joinComma(textLines);
            let $links = $event.find('.description a').map((i, $e) => $loadElement($e)).toArray();
            for (const $link of $links) {
                event.links.push({label: $link.text(), url: $link.attr('href')!});
            }
            if (genres) event.genres.add(genres);
            return event.build();
        });
    }
}
