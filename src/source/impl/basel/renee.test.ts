import {Renee} from "./renee";
import {testFetchRequest} from "test/testFetchRequest";
import {nockBack} from "test/nockBack";

describe('Renée', () => {
    test('renee-2022-11-23', async () => {
        await nockBack('renee-2022-11-23', async () => {
            let source = new Renee();
            let events = await source.fetch(testFetchRequest('2022-11-23'));
            expect(events).toMatchSnapshot();
        });
    });
});
