import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import {joinDot, matchForce} from "source/textHelper";
import {Moment} from "moment-timezone";
import {$load, $loadElement} from "../../cheerio/load";


export class Ubwg implements Source {
    private _regionVariant: RegionVariant;
    private city: string;


    constructor(regionVariant: RegionVariant, city: string) {
        this._regionVariant = regionVariant;
        this.city = city;
    }

    sourceName(): string {
        return `ubwg-city:${this.city}`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.ListingWebsite;
    }

    regionVariant(): RegionVariant {
        return this._regionVariant;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [
            new Ubwg(RegionVariant.zurich, 'zuerich'),
            new Ubwg(RegionVariant.basel, 'basel'),
        ];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        // ubwg.ch can't really handle parallel requests at all, leading to congestion and occasional request timeouts
        // For now we request sequentially, which takes the same (long) period of time without risking timeouts
        let eventsByDay = [];
        for (let date of request.dates) {
            const eventData = await this._fetchDate(date, request);
            eventsByDay.push(eventData);
        }
        return eventsByDay.flat();
    }

    async _fetchDate(date: Moment, request: FetchRequest): Promise<EventData[]> {
        let dateString = date.format('YYYY-MM-DD');
        let url = `https://ubwg.ch/events/kategorie/${this.city}/${dateString}/`;
        let html = await fetchUrl(url);

        let $ = $load(html);
        let events = $('.tribe-events .type-tribe_events').map((i, $e) => $loadElement($e)).toArray();
        return events.map($event => {
            let eventUrl = $event.find('.tribe-events-calendar-day__event-title a').attr('href')!;
            let event = new EventDataBuilder(request.now, eventUrl);

            event.venueName = $event.find('.ubwg-event-link-text').textVisual();

            const title = $event.find('.tribe-events-calendar-day__event-title').textVisual();

            const descriptionTitle = $event.find('.ubwg-events-lineup h2').textVisual();
            const lineupItems = $event.find('.ubwg-events-lineup ul li span');
            const lineup = lineupItems.map((i, el) => $loadElement(el).textVisual()).toArray().join(', ');
            const description = `${descriptionTitle} ${lineup}`;

            event.description = joinDot([title, description]);

            const cleanDate = (text: string) : string => {
                return text.replaceAll("Mitternacht", "24:00");
            };
            const from = cleanDate($event.find('.tribe-event-date-start').textVisual());
            const until = cleanDate($event.find('.tribe-event-date-end').textVisual());

            const dateRegExp = /^(?<weekday>\p{L}{2,15})?,? *(?<day>\d{1,2})\. (?<month>\p{L}{2,15}) *(?<year>\d{4})? *(·? *(ab)? )?(?<hour>\d{1,2}):(?<minute>\d{2})/u;

            let fromMatch = matchForce(
                from,
                dateRegExp
            );

            event.time.from.setDateByParts(fromMatch.groups['day'], fromMatch.groups['month'], fromMatch.groups['year']);
            event.time.from.setTimeByParts(fromMatch.groups['hour'], fromMatch.groups['minute']);

            if (until.length) {
                let untilMatch = matchForce(
                    until,
                    dateRegExp
                );

                if (untilMatch.groups['day']) {
                    event.time.until.setDateByParts(untilMatch.groups['day'], untilMatch.groups['month'], untilMatch.groups['year']);
                }
                event.time.until.setTimeByParts(untilMatch.groups['hour'], untilMatch.groups['minute']);
            }

            return event.build();
        });
    }

}
