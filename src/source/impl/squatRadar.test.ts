import {SquatRadar} from "./squatRadar";
import {RegionVariant} from "denkmal_data/region";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('squatradar', () => {

    test('squatradar-2020-06-06', async function () {
        await nockBack('squatradar-2020-06-06', async () => {
            let source = new SquatRadar(RegionVariant.zurich, 'Zurich');
            let events = await source.fetch(testFetchRequest('2020-06-06'));
            expect(events).toMatchSnapshot();
        });
    });

});
