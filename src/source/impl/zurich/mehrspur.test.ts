import {Mehrspur} from "./mehrspur";
import {testFetchRequest} from "test/testFetchRequest";
import {nockBack} from "test/nockBack";

describe('Mehrspur', () => {

    test('mehrspur-2021-10-16', async function () {
        await nockBack('mehrspur-2021-10-16', async () => {
            let source = new Mehrspur();
            let events = await source.fetch(testFetchRequest('2021-10-16'));
            expect(events).toMatchSnapshot();
        });
    });

});
