import {Kater} from "./kater";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";


describe('Kater', () => {

    test('kater-2021-07-25', async function () {
        await nockBack('kater-2021-07-25', async () => {
            let source = new Kater();
            let events = await source.fetch(testFetchRequest('2021-07-25'));
            expect(events).toMatchSnapshot();
        });
    });

    test('kater-2022-02-04', async function () {
        await nockBack('kater-2022-02-04', async () => {
            let source = new Kater();
            let events = await source.fetch(testFetchRequest('2022-02-04'));
            expect(events).toMatchSnapshot();
        });
    });

    test('kater-2022-11-17', async function () {
        await nockBack('kater-2022-11-17', async () => {
            let source = new Kater();
            let events = await source.fetch(testFetchRequest('2022-11-17'));
            expect(events).toMatchSnapshot();
        });
    });

});
