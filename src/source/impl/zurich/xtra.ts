import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import * as de from "@mojotech/json-type-validation";
import {uniqBy} from "ramda"
import {$load} from "../../../cheerio/load";

export class Xtra implements Source {

    sourceName(): string {
        return `xtra`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Xtra()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let categories = ['konzerte', 'parties'];
        let eventLists = await Promise.all(categories.map(c => fetchCategory(request, c)));
        let events = eventLists.reduce((acc, events) => acc.concat(events), []);
        events = uniqBy((e) => {
            return JSON.stringify([e.time, e.description]);
        }, events);
        return events;
    }

}

async function fetchCategory(request: FetchRequest, category: string): Promise<EventData[]> {
    let urlBase = 'https://www.x-tra.ch';
    let url = `${urlBase}/de/agenda/${category}/`;
    let html = await fetchUrl(url);

    let $ = $load(html);
    let json = $('script[type="application/ld+json"]').html() || '';
    json = fixJson(json);

    return parseSchema(json)
        .filter(e => e.startDate.length > 0)
        .map(e => {
            let event = new EventDataBuilder(request.now, url);
            event.venueName = e.location.name;
            event.time.from.setByISO8601(e.startDate);
            event.description = e.name;
            if (e.eventStatus) {
                event.description += ' - ' + e.eventStatus;
            }
            if (e.performer && e.performer.sameAs) {
                let performerUrl = e.performer.sameAs.trim();
                if (!performerUrl.startsWith('http')) {
                    performerUrl = 'http://' + performerUrl;
                }
                event.links.push({label: e.performer.name, url: performerUrl});
            }
            return event.build();
        });
}

function fixJson(json: string): string {
    json = json.replace(/("eventStatus"\s*:\s*"[^"]*")(\s*")/mg, '$1,$2');
    return json
}

function parseSchema(json: string): SchemaMusicEvent[] {
    let schema = JSON.parse(json);

    let decodeArray: de.Decoder<{}[]> = de.array(de.object());

    let decodeItem: de.Decoder<SchemaMusicEvent> = de.object({
        '@type': de.constant('MusicEvent'),
        name: de.string(),
        startDate: de.string(),
        eventStatus: de.optional(de.string()),
        location: de.object({
            '@type': de.constant('Place'),
            name: de.string(),
        }),
        performer: de.optional(de.object({
            '@type': de.constant('MusicGroup'),
            name: de.string(),
            sameAs: de.optional(de.string()),
        })),
    });

    return decodeArray.runWithException(schema)
        .map(item => decodeItem.runWithException(item));
}

type SchemaMusicEvent = {
    '@type': string,
    name: string,
    startDate: string,
    eventStatus?: string,
    location: {
        '@type': string,
        name: string,
    },
    performer?: {
        '@type': string,
        name: string,
        sameAs?: string,
    },
};
