import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import {$load, $loadElement} from "../../../cheerio/load";


export class Ellokal implements Source {

    sourceName(): string {
        return `ellokal`;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Ellokal()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let urlBase = 'http://www.ellokal.ch';
        let url = `${urlBase}/?lang=de&details=9`;
        let html = await fetchUrl(url);

        let $ = $load(html);
        let $events = $('#maincontent > .commingupEventsList_0,.commingupEventsList_1').map((i, $e) => $loadElement($e)).toArray();
        return $events.map($event => {
            let eventUrl = $event.find('.commingupEventsList_block5 a').attr('href')!;
            if (!eventUrl.match(/^http/)) {
                eventUrl = urlBase + '/' + eventUrl
            }
            let event = new EventDataBuilder(request.now, eventUrl);
            event.venueName = 'El Lokal';
            event.links.push({url: eventUrl, label: 'El Lokal'});

            event.time.from.setDateByParts(
                $event.find('.commingupEventsList_block2').textVisual().replace(/\.$/, ''),
                $event.find('.commingupEventsList_block3').textVisual(),
            );
            let timeText = $event.find('.commingupEventsList_block4').textVisual();
            if (timeText.length > 0) {
                event.time.from.setByMatch(
                    timeText,
                    /(?<hour>\d{1,2})(:|Uhr)(?<minute>\d{2})/i,
                );
            }

            event.description = $event.find('.commingupEventsList_block5').textVisual();

            return event.build();
        });
    }

}
