import {Xtra} from "./xtra";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('Xtra', () => {

    test('xtra-2019-04-20', async function () {
        await nockBack('xtra-2019-04-20', async () => {
            let source = new Xtra();
            let events = await source.fetch(testFetchRequest('2019-04-20'));
            expect(events).toMatchSnapshot();
        });
    });

    test('xtra-2019-05-03', async function () {
        await nockBack('xtra-2019-05-03', async () => {
            let source = new Xtra();
            let events = await source.fetch(testFetchRequest('2019-05-03'));
            expect(events).toMatchSnapshot();
        });
    });

    test('xtra-2019-10-20', async function () {
        await nockBack('xtra-2019-10-20', async () => {
            let source = new Xtra();
            let events = await source.fetch(testFetchRequest('2019-10-20'));
            expect(events).toMatchSnapshot();
        });
    });

    test('xtra-2020-11-13', async function () {
        await nockBack('xtra-2020-11-13', async () => {
            let source = new Xtra();
            let events = await source.fetch(testFetchRequest('2020-11-13'));
            expect(events).toMatchSnapshot();
        });
    });
});
