import {Hive} from "./hive";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";


describe('Hive', () => {

    test('hive-2021-09-19', async function () {
        await nockBack('hive-2021-09-19', async () => {
            let source = new Hive();
            let events = await source.fetch(testFetchRequest('2021-09-19'));
            expect(events).toMatchSnapshot();
        });
    });

    test('hive-2021-09-24', async function () {
        await nockBack('hive-2021-09-24', async () => {
            let source = new Hive();
            let events = await source.fetch(testFetchRequest('2021-09-24'));
            expect(events).toMatchSnapshot();
        });
    });

    test('hive-2021-10-16', async function () {
        await nockBack('hive-2021-10-16', async () => {
            let source = new Hive();
            let events = await source.fetch(testFetchRequest('2021-10-16'));
            expect(events).toMatchSnapshot();
        });
    });

    test('hive-2022-02-04', async function () {
        await nockBack('hive-2022-02-04', async () => {
            let source = new Hive();
            let events = await source.fetch(testFetchRequest('2022-02-04'));
            expect(events).toMatchSnapshot();
        });
    });

    test('hive-2022-11-19', async function () {
        await nockBack('hive-2022-11-19', async () => {
            let source = new Hive();
            let events = await source.fetch(testFetchRequest('2022-11-19'));
            expect(events).toMatchSnapshot();
        });
    });

    test('hive-2022-11-22', async function () {
        await nockBack('hive-2022-11-22', async () => {
            let source = new Hive();
            let events = await source.fetch(testFetchRequest('2022-11-22'));
            expect(events).toMatchSnapshot();
        });
    });

    test('hive-2022-11-24', async function () {
        await nockBack('hive-2022-11-24', async () => {
            let source = new Hive();
            let events = await source.fetch(testFetchRequest('2022-11-24'));
            expect(events).toMatchSnapshot();
        });
    });

});
