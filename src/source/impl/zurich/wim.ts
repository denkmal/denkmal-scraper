import {Cheerio, Element} from 'cheerio';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {joinComma} from "source/textHelper";
import {DenkmalRepo} from "denkmal_data/repo";
import {ParseError} from "error/parseError";
import {$load, $loadElement} from "../../../cheerio/load";


export class Wim implements Source {

    sourceName(): string {
        return `wim`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Wim()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = `https://wimmusic.ch/programm/`;
        let html = await fetchUrl(url);

        let $ = $load(html);
        let $rows = $('.tx-roevents-list > .row').map((i, $e) => $loadElement($e)).toArray();
        let dayEvents = extractDayEventsFromRows($rows);

        return dayEvents.map(dayEvent => {
            let event = new EventDataBuilder(request.now, url);
            event.venueName = 'Werkstatt für improvisierte Musik';

            event.time.from.setByMatch(dayEvent.date,
                /^\p{L}{2}, (?<day>\d{1,2})\. (?<month>\p{L}{2,20})$/u
            );
            event.time.from.setByMatch(dayEvent.time,
                /^(?<hour>\d{1,2}):(?<minute>\d{2})$/
            );

            event.description = joinComma(dayEvent.titles);
            event.links.push({label: 'WIM', url: dayEvent.url});

            return event.build();
        });
    }

}

type DayEvent = {
    date: string,
    time: string,
    url: string,
    titles: string[],
};

function extractDayEventsFromRows($rows: Cheerio<Element>[]): DayEvent[] {
    let dayEvents: DayEvent[] = [];
    for (let $row of $rows) {
        let $col1 = $row.find('.col-sm-2');
        let $strong = $col1.find('strong');
        let date = $strong.textVisual();
        $strong.remove();
        let time = $col1.textVisual();

        let $col2 = $row.find('.col-sm-10');
        let $link = $col2.find('a');
        let title = $link.text();
        let url = $link.attr('href')!;
        if (!url.match(/^http/)) {
            url = 'https://wimmusic.ch/' + url
        }

        if (date.length > 0) {
            dayEvents.push({
                date,
                time,
                url,
                titles: [title],
            })
        } else {
            let lastEvent = dayEvents[dayEvents.length - 1];
            if (!lastEvent) {
                throw new ParseError('No previous event to append row to', {
                    row: $row.html(),
                });
            }
            if (lastEvent.titles.length > 3) {
                throw new ParseError(`Previous event already has 3 titles, not appending more.`, {
                    titles: lastEvent.titles.join(', '),
                    row: $row.html(),
                });
            }
            lastEvent.titles.push(title);
        }
    }
    return dayEvents;
}
