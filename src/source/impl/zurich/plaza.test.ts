import {Plaza} from "./plaza";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('Plaza', () => {

    test('plaza-2020-01-24', async function () {
        await nockBack('plaza-2020-01-24', async () => {
            let source = new Plaza();
            let events = await source.fetch(testFetchRequest('2020-01-24'));
            expect(events).toMatchSnapshot();
        });
    });

    test('plaza-2020-02-11', async function () {
        await nockBack('plaza-2020-02-11', async () => {
            let source = new Plaza();
            let events = await source.fetch(testFetchRequest('2020-02-11'));
            expect(events).toMatchSnapshot();
        });
    });

    test('plaza-2022-11-17', async function () {
        await nockBack('plaza-2022-11-17', async () => {
            let source = new Plaza();
            let events = await source.fetch(testFetchRequest('2022-11-17'));
            expect(events).toMatchSnapshot();
        });
    });

});
