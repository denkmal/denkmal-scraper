import {EventData, EventDataBuilder, EventLink} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import * as de from "@mojotech/json-type-validation";


export class Rotefabrik implements Source {

    sourceName(): string {
        return `rotefabrik`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Rotefabrik()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let dateMin = request.dateMin();
        let dateMax = request.dateMax();
        let url = `https://kalender.rotefabrik.ch/api/events?from=${dateMin.format('YYYY-MM-DD')}&to=${dateMax.format('YYYY-MM-DD')}&limit=1000`;
        let text = await fetchUrl(url);
        let events = this.parseResponse(text);

        return events
            .filter(e => {
                return e.rf_event.categories.some(cat => {
                    return /(konzert|party)/i.test(cat.name)
                });
            })
            // Ignore invalid event on 2021-07-26
            .filter(e => {
                if (e.rf_event.title == "Summer Camp" && e.from == "00:00:00" && e.to == "00:00:00") {
                    return false
                }
                return true
            })
            .map(e => {
                let eventUrl = `https://rotefabrik.ch/de/programm.html#/events/${e.id}`;
                let event = new EventDataBuilder(request.now, eventUrl);
                event.venueName = `Rote Fabrik (${e.rf_event.location.name})`;
                event.time.from.setByISO8601(e.date);
                event.time.from.setByMatch(e.from, /^(?<hour>\d{1,2}):(?<minute>\d{2}):\d{2}$/u);
                event.time.until.setByMatch(e.to, /^(?<hour>\d{1,2}):(?<minute>\d{2}):\d{2}$/u);
                event.description = e.rf_event.title;
                event.links.push({label: 'rotefabrik', url: eventUrl});
                let links = e.rf_event.rf_event_links
                    .reduce((acc: EventLink[], l) => {
                        if (l.title && l.url) {
                            let linkUrl = l.url;
                            // Wrongly inserted URL in their CMS
                            if (!linkUrl.startsWith('http')) {
                                linkUrl = 'http://' + linkUrl;
                            }
                            acc.push({label: l.title, url: linkUrl});
                        }
                        return acc;
                    }, []);
                event.links.push(...links);
                return event.build();
            });
    }

    private parseResponse(json: any): EventNodeType[] {

        let decodeArray: de.Decoder<{ [key: string]: {} }> = de.dict(de.object());

        let decodeItem: de.Decoder<EventNodeType> = de.object({
            id: de.number(),
            date: de.string(),
            from: de.string(),
            to: de.string(),
            rf_event: de.object({
                title: de.string(),
                location: de.object({
                    name: de.string(),
                }),
                categories: de.array(
                    de.object({
                        name: de.string(),
                    })
                ),
                rf_event_links: de.array(
                    de.object({
                        title: de.oneOf(de.string(), de.constant(null)),
                        url: de.oneOf(de.string(), de.constant(null)),
                    })
                ),
            }),
        });

        let itemsObj = decodeArray.runWithException(json);
        return Object.values(itemsObj)
            .map(item => decodeItem.runWithException(item));
    }
}


type EventNodeType = {
    id: number,
    date: string,
    from: string,
    to: string,
    rf_event: {
        title: string,
        location: {
            name: string,
        },
        categories: {
            name: string,
        }[],
        rf_event_links: {
            title: string | null,
            url: string | null,
        }[],
    },
};
