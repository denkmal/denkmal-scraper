import {Ellokal} from "./ellokal";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('Ellokal', () => {

    test('ellokal-2019-04-20', async function () {
        await nockBack('ellokal-2019-04-20', async () => {
            let source = new Ellokal();
            let events = await source.fetch(testFetchRequest('2019-04-20'));
            expect(events).toMatchSnapshot();
        });
    });

});
