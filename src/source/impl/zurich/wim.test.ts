import {Wim} from "./wim";
import {testFetchRequest} from "test/testFetchRequest";
import {nockBack} from "test/nockBack";

describe('Wim', () => {

    test('wim-2022-11-19', async () => {
        await nockBack('wim-2022-11-19', async () => {
            let source = new Wim();
            let events = await source.fetch(testFetchRequest('2022-11-19'));
            expect(events).toMatchSnapshot();
        });
    });

});
