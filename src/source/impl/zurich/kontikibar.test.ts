import {Kontikibar} from "./kontikibar";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('Kontikibar', () => {

    test('kontikibar-2019-04-28', async function () {
        await nockBack('kontikibar-2019-04-28', async () => {
            let source = new Kontikibar();
            let events = await source.fetch(testFetchRequest('2019-04-28'));
            expect(events).toMatchSnapshot();
        });
    });

});
