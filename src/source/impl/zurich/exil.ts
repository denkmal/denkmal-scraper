import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import {$load, $loadElement} from "../../../cheerio/load";


export class Exil implements Source {

    sourceName(): string {
        return `exil`;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Exil()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = `https://exil.club/events`;
        let html = await fetchUrl(url);

        let $ = $load(html);
        let $events = $('.events-list > .list-event').map((i, $e) => $loadElement($e)).toArray();

        let eventBuilders = $events.flatMap(($event) => {
            let eventUrl = $event.attr('href')!;
            let event = new EventDataBuilder(request.now, eventUrl);
            event.venueName = 'Exil';
            event.links.push({label: event.venueName, url: eventUrl});

            // Title
            let title = $event.find('.title').textVisual();
            if (title.endsWith("im Moods")) {
                return [];
            }
            event.description = title;

            // Date
            let date = $event.find('.date').textVisual();
            event.time.from.setByMatch(
                date,
                /^(?<day>\d{1,2})\.(?<month>\d{1,2})\.(?<year>\d{2}) /
            );
            if (event.time.from.isAfter(request.dateMax())) {
                return [];
            }

            return [event];
        });

        return await Promise.all(eventBuilders.map(eventBuilder => {
            return this.fetchEvent(eventBuilder);
        }));
    }

    async fetchEvent(event: EventDataBuilder): Promise<EventData> {
        let html = await fetchUrl(event.sourceUrl);
        let $ = $load(html);

        let timeHasEinlass = event.time.from.setByMatchOptional(
            $('.content .door').textVisual(),
            /^Einlass: (?<hour>\d{1,2}):(?<minute>\d{2}) Uhr$/u,
        );
        if (!timeHasEinlass) {
            event.time.from.setByMatch(
                $('.content .start').textVisual(),
                /^Start: (?<hour>\d{1,2}):(?<minute>\d{2}) Uhr$/u,
            );
        }

        return event.build();
    }

}
