import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import {joinColon, joinComma} from "source/textHelper";
import {ParseError} from "error/parseError";
import {$load, $loadElement} from "../../../cheerio/load";

export class Kater implements Source {

    sourceName(): string {
        return `kater`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Kater()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = `http://www.kater-rockt.ch/`;
        let html = await fetchUrl(url);
        let $ = $load(html);

        return $('#divEvents > p.pZeitenZeile').map((i, $e) => $loadElement($e)).toArray()
            .filter($row => {
                let text = $row.textVisual();
                return text.length > 0
                    && text != "Programm"
                    && text.match(/Ab \d{1,2}:\d{1,2} Live:/ui)
            })
            .map($event => {
                let event = new EventDataBuilder(request.now, url);
                event.venueName = 'Kater';

                // Link
                let $link = $event.find('a');
                if ($link.length > 0) {
                    let url = $link.attr('href')!;
                    let label = 'event';
                    if (url.includes('facebook.com')) {
                        label = 'facebook';
                    }
                    event.links.push({label, url});
                }

                // Text
                let text = $event.textVisual();
                let lines = text.split(/\n+/)
                    .filter(line => !line.includes("Eintritt nur mit gültigem Covid-Zertifikat"))

                let line1 = lines.shift() || '';
                if (line1.toLocaleLowerCase() == 'neues datum') {
                    line1 = lines.shift() || '';
                }
                event.time.from.setByMatch(
                    line1,
                    /^(\p{L}{2} )?(?<day>\d{1,2})\.\s*(?<month>\p{L}{2,20}) (?<year>\d{4})\b/u,
                );

                let line2 = lines.shift() || '';
                let patternTime = /^\s*Ab (?<hour>\d{1,2}):(?<minute>\d{2}) Live:\s*/u;
                event.time.from.setByMatch(line2, patternTime);
                let title = line2.replace(patternTime, '');
                if (!title) {
                    title = lines.shift() || '';
                }
                if (!title) {
                    throw new ParseError('Cannot detect event title', {text});
                }
                event.description = joinColon([title, joinComma(lines)]);

                return event.build();
            });
    }

}
