import {Dynamo} from "./dynamo";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('Dynamo', () => {

    test('dynamo-2020-01-24', async function () {
        await nockBack('dynamo-2020-01-24', async () => {
            let source = new Dynamo();
            let events = await source.fetch(testFetchRequest('2020-01-24'));
            expect(events).toMatchSnapshot();
        });
    });

});
