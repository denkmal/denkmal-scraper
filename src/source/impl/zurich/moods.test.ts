import {Moods} from "./moods";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('Moods', () => {

    test('moods-2022-11-17', async function () {
        await nockBack('moods-2022-11-17', async () => {
            let source = new Moods();
            let events = await source.fetch(testFetchRequest('2022-11-17'));
            expect(events).toMatchSnapshot();
        });
    });

});
