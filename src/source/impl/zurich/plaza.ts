import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import {$load, $loadElement} from "../../../cheerio/load";


export class Plaza implements Source {

    sourceName(): string {
        return `plaza`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Plaza()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = `https://www.plaza-zurich.ch/nu/events/event_list_type/2`;
        let html = await fetchUrl(url);

        let $ = $load(html);
        let $events = $('.nu-event').map((i, $e) => $loadElement($e)).toArray();
        return $events.flatMap($event => {
            let event = new EventDataBuilder(request.now, url);
            event.venueName = 'Plaza';

            // Title
            let title = $event.find('h2')
                .find('span').remove().end()
                .textVisual();
            event.description = title;

            // Description
            let description = $event.find('.nu-e-shortdescription').textVisual();
            // Use a default time (19:00) in case there's no time
            event.time.from.setTime([19, 0]);
            event.time.from.setByMatchOptional(description,
                /\b(?<hour>\d{1,2})([.:](?<minute>\d{2}))?(h|(?<ampm>[amp]{2}))\b/u
            );

            // Date
            let date = $event.find('.nu-e-date').textVisual();
            event.time.from.setByMatch(date, /\b\p{L}{2} (?<day>\d{1,2}) ?(?<month>\p{L}{2,4})\b/u);

            return [event.build()];
        });
    }

}
