import {AnyNode, Cheerio, Document} from "cheerio";
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {joinComma, joinDot, matchForce, matchOptional, splitForce, stringIncludesAnyCase} from "source/textHelper";
import {ParseError} from "error/parseError";
import {$load, $loadElement} from "../../../cheerio/load";
import {DenkmalRepo} from "denkmal_data/repo";
import {YearMonth} from "source/eventTimeBuilder";


const patternWeekdays = 'montag|dienstag|mittwoch|donnerstag|freitag|samstag|sonntag|mo|di|mi|do|fr|sa|so';
const patternDate = `(?:${patternWeekdays})\\s+\\d{1,2}`;


export class Kauz implements Source {

    sourceName(): string {
        return `kauz`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Kauz()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = `https://www.kauzig.ch/`;
        let html = await fetchUrl(url);

        let eventBuilderFactory: EventBuilderFactory = function () {
            let event = new EventDataBuilder(request.now, url);
            event.venueName = 'Kauz';
            return event;
        }

        let contentBlocks = extractContentBlocks(html);
        let agendaBlocks = contentBlocks.map(contentBlock => extractAgendaBlock(contentBlock));
        return processMultipleAgendaBlocks(agendaBlocks, eventBuilderFactory);
    }
}


type ContentBlock = { title: string, content: string };

/**
 * Extract main content blocks on the website, separated by titles
 */
function extractContentBlocks(html: string): ContentBlock[] {
    let htmlContent = $load(html).root().find('#home > div').html() || '';

    // Remove footer
    htmlContent = htmlContent.replace(/<h1>Kauz<\/h1>.+$/s, '');

    // Bail if Summer break
    if (matchOptional(htmlContent, /<h1>SOMMER<\/h1>/)) {
        return [];
    }

    // Find titles
    let titleFramePattern = String.raw`(?:\n*<p>(?:\s|&#x30FB;|・)+<\/p>\n*)`
    let titleMainPattern = String.raw`(?:(?:<h1>.+?<\/h1>\s*)+|<h2>.*?SOMMER CLOSING.*?<\/h2>)`
    let titleRegex = new RegExp(`(?:${titleFramePattern})?(?<title>${titleMainPattern})(?:${titleFramePattern})?`, 'gi');
    let titleMatches = htmlContent.split(titleRegex);
    titleMatches.shift();

    let titleContentPairs: ContentBlock[] = [];
    while (titleMatches.length >= 2) {
        let group1 = titleMatches.shift() || '';
        let group2 = titleMatches.shift() || '';
        titleContentPairs.push({title: group1, content: group2});
    }
    titleContentPairs = titleContentPairs.filter(titleContentPair => {
        let excludedTitles = [
            'SOMMER CLOSING',
            'Kauz Sommerbar',
            'Bosefs Taverne',
            'COVID-19',
        ];
        return !excludedTitles.some(excludedTitle => titleContentPair.title.includes(excludedTitle))
    });
    if (titleContentPairs.length < 1) {
        throw new ParseError(`Found less than one titles-content-pairs`, {html});
    }
    return titleContentPairs;
}


type AgendaBlock = { title: string, agendaItems: AgendaItem[] };
type AgendaItem = { date: string, titles: string[], contents: string[] };

/**
 * Extract raw event information from agenda content blocks
 */
function extractAgendaBlock(contentBlock: ContentBlock): AgendaBlock {
    let title = $load(contentBlock.title).root().textVisual();
    let contentHtml = contentBlock.content;

    // Combine multiple `<h1>` into one
    contentHtml = contentHtml
        .replace(/(<h[12]>)(.+?)(<\/h[12]>)[\n\s]*<h[12]>(.+?)<\/h[12]>/g, '$1$2 $4$3');

    let $contentDoc = $load(contentHtml);
    let $content : Cheerio<Document>|Cheerio<AnyNode>  = $contentDoc.root();

    // Remove `<small>`, except for date titles
    // Usually contains country or other details of acts, which we are not interested in
    $content = $content
        .find('small')
        .filter((i: any, el: any) => {
            let text = $contentDoc(el).text();
            let isDate = new RegExp(`^${patternDate}$`, 'i');
            return !isDate.test(text);
        })
        .remove()
        .end().end();

    // Remove `<blockquote>`
    // Usually contains details about events, which we are not interested in
    $content = $content
        .find('blockquote')
        .remove()
        .end();

    let agendaItems = extractAgendaItems($content);
    return {title, agendaItems};
}


function extractAgendaItems($block: Cheerio<AnyNode>): AgendaItem[] {
    let blockHtml = $block.html() || '';

    // Remove HTML comments, so it doesn't show up when parsing HTML with regex
    blockHtml = blockHtml.replace(/<!--.*?-->/s, '')

    // Split the agenda block HTML by date header
    let itemsHtml = splitForce(blockHtml, new RegExp(`(?=(?:<[^>]+>)(?:${patternDate})(?:</[^>]+>))`, 'i'));

    // Create new HTML docs for each agenda item
    itemsHtml.shift();
    let $items = itemsHtml.map(html => $load(html).root())

    return $items
        .map($item => {
            const itemh = $item.html();
            // Extract date header (wrapped in `<small>`)
            let $date = $item
                .find('small')
                .filter((index, $el) => {
                    let text = $loadElement($el).textVisual();
                    let match = matchOptional(text, new RegExp(`(?<date>${patternDate})`, 'i'))
                    return !!match;
                })
                .remove();
            let dateMatch = matchForce($date.textVisual(), new RegExp(`(?<date>${patternDate})`, 'i'));
            let date = dateMatch.groups['date'];

            // Extract item header (wrapped in `<h2>`)
            let $headers = $item
                .find('h2')
                .remove();
            let titles = $headers.toArray()
                .map($header => $loadElement($header).textVisual());

            // Parse the rest of the item
            let text = $item.textVisual();
            let contents = text
                .trim()
                .replace(/\n\s+\n/g, '\n\n')
                .split(/\n+/);
            if (contents.length < 1) {
                throw new ParseError('Item has less than 1 lines', {text});
            }
            return {date, titles, contents};
        });
}


type EventBuilderFactory = () => EventDataBuilder;

/**
 * Convert multiple agenda blocks (usually one per month) to events
 */
function processMultipleAgendaBlocks(agendaBlocks: AgendaBlock[], eventBuilderFactory: EventBuilderFactory): EventData[] {
    let events = [];
    for (let agendaBlock of agendaBlocks) {
        let {title, agendaItems} = agendaBlock;
        let month = parseAgendaBlockTitle(title);
        let dayPrevious;

        agendaItems = agendaItems
            .filter(item => !item.contents[0].match(/^GESCHLOSSEN$/i))
            .filter(item => !item.titles[0].match(/^GESCHLOSSEN$/i));

        for (let agendaItem of agendaItems) {
            let event = eventBuilderFactory();

            let dateMatch = matchForce(agendaItem.date, /^\p{L}+\s+(\d{1,2})$/u);
            let day = parseInt(dateMatch[1]);
            if (day == 31 && month.month == 9 && month.year == 2019) {
                // Special case: last day of August is part of September program
                month.month = 8;
            }
            if (dayPrevious && dayPrevious > day) {
                // Event is from *next* month
                month.incrMonth();
            }
            dayPrevious = day;
            event.time.from.setDateByParts(day, month.month, month.year);
            event.time.from.setTimeByParts(23, 0);

            let contents = agendaItem.contents
                .map(l => l.trim())
                .filter(l => l.length > 1)
                .map(l => l.replace(/\s+[*_]\s+live$/, ' (live)'));

            // Lines ending on " mit" are joined with the next line
            while (true) {
                let lineWithMit = contents.findIndex(l => !!l.match(/\smit$/i));
                if (lineWithMit === -1 || lineWithMit >= contents.length - 1) {
                    break;
                }
                contents[lineWithMit] += ' ' + contents[lineWithMit + 1];
                contents.splice(lineWithMit + 1, 1);
            }

            // Remove title-lines that are already contained in content-lines
            let titles = agendaItem.titles;
            titles = titles.filter(content => {
                return !contents.some(title => {
                    return stringIncludesAnyCase(content, title);
                })
            })

            // Build final description from titles and contents
            let description = joinComma(contents);
            if (titles.length > 0) {
                description = joinDot([joinComma(titles), description]);
            }
            event.description = description;

            events.push(event.build());
        }
    }
    return events;
}

function parseAgendaBlockTitle(title: string): YearMonth {
    let titleMatch1 = matchOptional(title, /^(\p{L}+)\s+(\d{4})$/u);
    if (titleMatch1) {
        return new YearMonth(titleMatch1[1], titleMatch1[2]);
    }

    let titleMatch2 = matchOptional(title, /^(\p{L}+)\s+'(\d{2})$/u);
    if (titleMatch2) {
        return new YearMonth(titleMatch2[1], `20${titleMatch2[2]}`);
    }

    throw new ParseError(`Cannot parse agenda-block-title: '${title}'`);
}
