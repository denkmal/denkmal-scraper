import {Provitreff} from "./provitreff";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('Provitreff', () => {

    test('Provitreff-2019-04-20', async () => {
        await nockBack('Provitreff-2019-04-20', async () => {
            let source = new Provitreff();
            let events = await source.fetch(testFetchRequest('2019-04-20'));
            expect(events).toMatchSnapshot();
        });
    });

});
