import {Helsinki} from "./helsinki";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('helsinki', () => {

    test('helsinki-2019-12-24', async () => {
        await nockBack('helsinki-2019-12-24', async () => {
            let source = new Helsinki();
            let events = await source.fetch(testFetchRequest('2019-12-24'));
            expect(events).toMatchSnapshot();
        });
    });

    test('helsinki-2021-07-25', async () => {
        await nockBack('helsinki-2021-07-25', async () => {
            let source = new Helsinki();
            let events = await source.fetch(testFetchRequest('2021-07-25'));
            expect(events).toMatchSnapshot();
        });
    });

});
