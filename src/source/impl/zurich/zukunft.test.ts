import {Zukunft} from "./zukunft";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";


describe('Zukunft', () => {

    test('zukunft-2019-03-23', async function () {
        await nockBack('zukunft-2019-03-23', async () => {
            let source = new Zukunft();
            let events = await source.fetch(testFetchRequest('2019-03-23'));
            expect(events).toMatchSnapshot();
        });
    });

});
