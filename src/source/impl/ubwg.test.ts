import {Ubwg} from "./ubwg";
import {nockBack} from "test/nockBack";
import {RegionVariant} from "denkmal_data/region";
import {testFetchRequest} from "test/testFetchRequest";


describe('Ubwg', () => {

    test('ubwg-2024-12-20', async function () {
        await nockBack('ubwg-2024-12-20', async () => {
            let source = new Ubwg(RegionVariant.zurich, 'zuerich');
            let events = await source.fetch(testFetchRequest('2024-12-20'));
            expect(events).toMatchSnapshot();
        });
    });

    test('ubwg-2024-12-23', async function () {
        await nockBack('ubwg-2024-12-23', async () => {
            let source = new Ubwg(RegionVariant.zurich, 'zuerich');
            let events = await source.fetch(testFetchRequest('2024-12-23'));
            expect(events).toMatchSnapshot();
        });
    });

});
