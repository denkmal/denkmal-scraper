import {EventTime} from "./eventTime";

describe('EventTime', () => {

    test('From', () => {
        let eventTime = new EventTime('Europe/Zurich',
            {date: [2019, 1, 3]}
        );
        expect(eventTime.fromAsISO8601WithDefaultTime()).toEqual('2019-01-03T12:00:00+01:00');
        expect(eventTime.untilAsISO8601WithDefaultTime()).toBeUndefined();
        expect(eventTime.toStringDebug()).toEqual('2019-01-03 (Europe/Zurich)');
        expect(eventTime.hasTime()).toEqual(false);
    });

    test('From-with-time', () => {
        let eventTime = new EventTime('Europe/Zurich',
            {date: [2019, 1, 3], time: [20, 30]}
        );
        expect(eventTime.fromAsISO8601WithDefaultTime()).toEqual('2019-01-03T20:30:00+01:00');
        expect(eventTime.untilAsISO8601WithDefaultTime()).toBeUndefined();
        expect(eventTime.toStringDebug()).toEqual('2019-01-03 20:30 (Europe/Zurich)');
        expect(eventTime.hasTime()).toEqual(true);
    });

    test('From and Until', () => {
        let eventTime = new EventTime('Europe/Zurich',
            {date: [2019, 1, 3], time: [20, 30]},
            {date: [2019, 1, 3], time: [23, 0]}
        );
        expect(eventTime.fromAsISO8601WithDefaultTime()).toEqual('2019-01-03T20:30:00+01:00');
        expect(eventTime.untilAsISO8601WithDefaultTime()).toEqual('2019-01-03T23:00:00+01:00');
        expect(eventTime.toStringDebug()).toEqual('2019-01-03 20:30 - 2019-01-03 23:00 (Europe/Zurich)');
        expect(eventTime.hasTime()).toEqual(true);
    });

    test('From-with-time and Until-without', () => {
        let eventTime = new EventTime('Europe/Zurich',
            {date: [2019, 1, 3], time: [20, 30]},
            {date: [2019, 1, 4]}
        );
        expect(eventTime.fromAsISO8601WithDefaultTime()).toEqual('2019-01-03T20:30:00+01:00');
        expect(eventTime.untilAsISO8601WithDefaultTime()).toEqual('2019-01-04T12:00:00+01:00');
        expect(eventTime.toStringDebug()).toEqual('2019-01-03 20:30 - 2019-01-04 (Europe/Zurich)');
        expect(eventTime.hasTime()).toEqual(false);
    });

    test('From-without and Until-with-time', () => {
        let eventTime = new EventTime('Europe/Zurich',
            {date: [2019, 1, 3]},
            {date: [2019, 1, 4], time: [23, 0]}
        );
        expect(eventTime.fromAsISO8601WithDefaultTime()).toEqual('2019-01-03T12:00:00+01:00');
        expect(eventTime.untilAsISO8601WithDefaultTime()).toEqual('2019-01-04T23:00:00+01:00');
        expect(eventTime.toStringDebug()).toEqual('2019-01-03 - 2019-01-04 23:00 (Europe/Zurich)');
        expect(eventTime.hasTime()).toEqual(false);
    });

    test('Time 24:00 is next day', () => {
        let eventTime = new EventTime('Europe/Zurich',
            {date: [2019, 1, 1], time: [24, 0]},
            {date: [2019, 1, 2], time: [24, 0]}
        );
        expect(eventTime.fromAsISO8601WithDefaultTime()).toEqual('2019-01-02T00:00:00+01:00');
        expect(eventTime.untilAsISO8601WithDefaultTime()).toEqual('2019-01-03T00:00:00+01:00');
        expect(eventTime.toStringDebug()).toEqual('2019-01-02 00:00 - 2019-01-03 00:00 (Europe/Zurich)');
    });

    test('Invalid date', () => {
        expect(() => {
            new EventTime('Europe/Zurich',
                {date: [2019, 1, 3], time: [99, 30]}
            );
        }).toThrow(/DateTime is invalid: '{ date: \[ 2019, 1, 3 ], time: \[ 99, 30 ] }'/);
    });

    test('Until before From', () => {
        expect(() => {
            new EventTime('Europe/Zurich',
                {date: [2019, 1, 3], time: [20, 30]},
                {date: [2019, 1, 3], time: [19, 30]}
            );
        }).toThrow(/Until-date must be after from-date./);
    });

    test('Until is same date-only as From', () => {
        expect(() => {
            new EventTime('Europe/Zurich',
                {date: [2019, 1, 3]},
                {date: [2019, 1, 3]}
            );
        }).toThrow(/Until-date must be after from-date./);
    });
});
