Denkmal Scraper
===============

This program downloads event data from various sources, then uploads it to
the [Denkmal API](https://gitlab.com/denkmal/denkmal-api).


Configure and Run
-----------------

Configure the program in `config/config.toml`.
The following environment variables are referenced in the config file:

- `DENKMAL_API_TOKEN`: Backend API token.
- `ELASTICSEARCH_URL`: Elasticsearch URL for logging. Use an empty string to disable logging.

Then run the program with:

```
yarn run scraper
```

Development
-----------

Install dependencies

```
yarn install
```

Run all tests:

```
yarn test
```

For testing purposes it can be useful to run the program locally.
For example run the source named "xtra" once:

```
yarn run scraper --one-shot --filter 'xtra'
```

```
Usage: scraper [options]

Options:
  -c, --config <file>     Path to config file (default: "config/config.toml")
  --one-shot              Run once, then exit (instead of running indefinitely).
  --dry-run               Only fetch, but don't upload events.
  --filter <source-name>  Filter by source name (e.g. "xtra").
```

### Developing Scraper Sources

Each source (a file in `src/source/impl/`) represents a remote source that is used to fetch events from.
See the `Source` interface for details about the required functions to implement.

##### Record&Replay Tests

Scraper tests use the `nockBack()` function to record&replay HTTP requests automatically.
Make sure to provide a _unique_ fixture name as the first argument.

Example (`kater.test.ts`):

```typescript
test('kater-2019-03-23', async function () {
    await nockBack('kater-2019-03-23', async () => {
        let source = new Kater();
        let events = await source.fetch(testFetchRequest('2019-03-23'));
        expect(events).toMatchSnapshot();
    }, true);
});
```

Steps for adding a Record&Replay test:
1. Add the test case (see example above). Set the third argument of `nockBack()` to `true` to enable recording of HTTP requests.
2. Run the test, which will record all HTTP requests. This will produce a file in `__nocks__` with the recorded HTTP requests, and will store the snapshot of the produced event data in `__snapshots__`. 
3. Check that the produced snapshot contains the correct event data.
4. Remove the `true` argument of `nockBack()` to enable _replaying_ of requests. You can now re-run the test, which will mock all HTTP requests using the recorded data.


Production Deployment
---------------------

Gitlab CI is deploying the application to Heroku:
https://dashboard.heroku.com/apps/denkmal-scraper
